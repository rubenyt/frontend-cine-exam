import { DomSanitizer } from '@angular/platform-browser';
import { ClienteService } from './../../_service/cliente.service';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario: string;
  roles: string[];
  imagenData: any;
  imagenEstado: boolean = false;
  idUsuario: number;

  constructor(private clienteService: ClienteService, private sanitization: DomSanitizer) { }

  ngOnInit() {

    const helper = new JwtHelperService();

    let token = JSON.parse(sessionStorage.getItem(environment.TOKEN_NAME));
    const decodedToken = helper.decodeToken(token.access_token);
    this.usuario = decodedToken.nombres + ' ' + decodedToken.apellidos;
    this.idUsuario = decodedToken.idus;
    this.roles = decodedToken.authorities;
    this.clienteService.listarPorId(this.idUsuario).subscribe(data=>{
      this.convertir(data);
    })

  }

  convertir(data: any) {
    if(data.size>0){
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let x = reader.result;
      this.setear(x);                
    }
    
    }else{
      this.setearDefault();
    }
  }

  setear(x: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(x);
    this.imagenEstado = true;
  }

  setearDefault(){
    this.imagenData = "https://cdn4.iconfinder.com/data/icons/evil-icons-user-interface/64/avatar-512.png";
    this.imagenEstado = true;
  }


}
