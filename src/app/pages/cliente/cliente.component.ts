import { ClienteDialogoComponent } from './cliente-dialogo/cliente-dialogo.component';
import { Cliente } from './../../_model/cliente';
import { ClienteService } from './../../_service/cliente.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  cantidad: number = 0;
  dataSource: MatTableDataSource<Cliente>;
  displayedColumns = ['idCliente', 'nombres','apellidos', 'dni', 'acciones'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private clienteService: ClienteService, private dialog:MatDialog, private snackBar:MatSnackBar) { }

  ngOnInit() {

    this.clienteService.clienteCambio.subscribe(data=>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    //listar
    this.clienteService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });

    this.clienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'INFO', {
        duration: 2000
      });
    });

  }


  openDialog(genero ?: Cliente){

    let gen = genero != null ? genero : new Cliente();
    this.dialog.open(ClienteDialogoComponent,{
      width: '250px',
      data: gen
    })

  }

  eliminar(cliente: Cliente){
    if(confirm('Desea eliminar este cliente?')){
    this.clienteService.eliminar(cliente.idCliente).subscribe(data=>{
      this.clienteService.listar().subscribe(clientes => {
        this.clienteService.clienteCambio.next(clientes);
        this.clienteService.mensajeCambio.next("Cliente eliminado");
      })
    })

  }
    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  mostrarMas(e : any){    
    //console.log(e);
    this.clienteService.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      //console.log(data);

      let clientes = data.content;
      this.cantidad = data.totalElements;
      
      this.dataSource = new MatTableDataSource(clientes);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }


}
